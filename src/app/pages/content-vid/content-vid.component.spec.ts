import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentVidComponent } from './content-vid.component';

describe('ContentVidComponent', () => {
  let component: ContentVidComponent;
  let fixture: ComponentFixture<ContentVidComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentVidComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentVidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
