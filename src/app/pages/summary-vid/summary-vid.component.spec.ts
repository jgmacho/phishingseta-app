import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryVidComponent } from './summary-vid.component';

describe('SummaryVidComponent', () => {
  let component: SummaryVidComponent;
  let fixture: ComponentFixture<SummaryVidComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SummaryVidComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryVidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
