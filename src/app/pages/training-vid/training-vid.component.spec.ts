import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingVidComponent } from './training-vid.component';

describe('TrainingVidComponent', () => {
  let component: TrainingVidComponent;
  let fixture: ComponentFixture<TrainingVidComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrainingVidComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingVidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
