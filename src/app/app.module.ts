import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterialModule } from './material.module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainNavComponent } from './main-nav/main-nav.component';
import { QuizPageComponent } from './pages/quiz-page/quiz-page.component';
import { LessonPageComponent } from './pages/lesson-page/lesson-page.component';
import { CommentsPageComponent } from './pages/comments-page/comments-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { PageOneComponent } from './pages/lesson-page/page-one/page-one.component';
import { PageTwoComponent } from './pages/lesson-page/page-two/page-two.component';
import { PageThreeComponent } from './pages/lesson-page/page-three/page-three.component';
import { PageFourComponent } from './pages/lesson-page/page-four/page-four.component';
import { PageFiveComponent } from './pages/lesson-page/page-five/page-five.component';
import { IntroVidComponent } from './pages/intro-vid/intro-vid.component';
import { TrainingVidComponent } from './pages/training-vid/training-vid.component';
import { SummaryVidComponent } from './pages/summary-vid/summary-vid.component';
import { ContentVidComponent } from './pages/content-vid/content-vid.component';

@NgModule({
  declarations: [
    AppComponent,
    MainNavComponent,
    QuizPageComponent,
    LessonPageComponent,
    CommentsPageComponent,
    HomePageComponent,
    PageOneComponent,
    PageTwoComponent,
    PageThreeComponent,
    PageFourComponent,
    PageFiveComponent,
    IntroVidComponent,
    TrainingVidComponent,
    SummaryVidComponent,
    ContentVidComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
