import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from 'src/app/pages/home-page/home-page.component';
import { LessonPageComponent } from 'src/app/pages/lesson-page/lesson-page.component';
import { QuizPageComponent } from 'src/app/pages/quiz-page/quiz-page.component';
import { CommentsPageComponent } from 'src/app/pages/comments-page/comments-page.component';
import { PageOneComponent } from 'src/app/pages/lesson-page/page-one/page-one.component';
import { PageTwoComponent } from 'src/app/pages/lesson-page/page-two/page-two.component';
import { PageThreeComponent } from 'src/app/pages/lesson-page/page-three/page-three.component';
import { PageFourComponent } from 'src/app/pages/lesson-page/page-four/page-four.component';
import { PageFiveComponent } from 'src/app/pages/lesson-page/page-five/page-five.component';
import { IntroVidComponent } from 'src/app/pages/intro-vid/intro-vid.component';
import { SummaryVidComponent } from 'src/app/pages/summary-vid/summary-vid.component';
import { ContentVidComponent } from 'src/app/pages/content-vid/content-vid.component';

const routes: Routes = [
  { path: 'home-page', component: HomePageComponent},
  { path: 'lesson-page', component: LessonPageComponent},
  { path: 'quiz-page', component: QuizPageComponent},
  { path: 'comments-page', component: CommentsPageComponent},
  { path: 'lesson-page/page-one', component: PageOneComponent},
  { path: 'lesson-page/page-two', component: PageTwoComponent},
  { path: 'lesson-page/page-three', component: PageThreeComponent},
  { path: 'lesson-page/page-four', component: PageFourComponent},
  { path: 'lesson-page/page-five', component: PageFiveComponent},
  { path: 'intro-vid', component: IntroVidComponent},
  { path: 'summary-vid', component: SummaryVidComponent},
  { path: 'content-vid', component: ContentVidComponent},
  { path: '',   redirectTo: '/home-page', pathMatch: 'full' },
  { path: '**', component: HomePageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
