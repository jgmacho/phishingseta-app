# IASM-8302 – Our Lady of the Lake University

## Team:
Jeffrey Camacho
Elva Guerrero

## SETA website:

https://master.d16jhb0h0tv3an.amplifyapp.com

## PhishingsetaApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.2.0.

## Project:

Our team decided to implement a SETA training module via a web application. It was chosen because of the flexibility to design and deliver various components using open source software. Our project components are as follows:

        •	Angular (Front-end framework: Javascript)
        •	AWS: Amplify framework to deploy and host webapp
        •	Youtube: Video hosting site for webapp videos
        •	Gitlab: Code repository for webapp
        •	Google Forms: Quiz software
        
Our team utilized agile methodologies to deliver each component into a live production environment.

## Repository:

https://gitlab.com/jgmacho/phishingseta-app

## Attached:

Zip file with screenshots of all pages of webapp
